# Bootcamp challenge



## Question 1

### If you are given a list or array of five (5) positive integers,WRITE A FUNCTION that:

* Finds the minimum and maximum values that can be calculated by summing
exactlyfour (4) out of the five (5) integers.
* Then print the respective minimum and maximum values as a single line of two
space-separated long integers.

```
 If numbers = [6,2,3,4,5]
    numbers.sort()
    console.log(minValue)
else
    console.log(maxValue)

```
## Question 2

### There is a sequence of words in [CamelCase](https://www.unf.edu/~broggio/cop2221/2221pseu.htm) as a string of letters, having the following properties:

*  It is a concatenation of one or more words consisting of English letters
* All letters in the first word are lowercase.
*  For each of the subsequent words, the first letter is uppercase and the rest of the
letters are lowercase.

### If you are given a CamelCase “word”,WRITE A FUNCTION that determines the number of words in the CamelCase
